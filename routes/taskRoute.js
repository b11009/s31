// Contains defines when a particular controller will be used

// contains endpoint
const express = require("express");

// naglalaman ng mga HTTP method
// Create a Router instance that functions as a routing system
const router = express.Router()

// import the task controller 
const taskController = require("../controllers/taskController");

// Route to get all tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send (
		resultFromController));
})

// Route to create a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send (
		resultFromController));
})

// Route to create delete a task
// endpoint: localhost:3001/tasks/123456
router.delete("/:id", (req, res) => {
	// retrieve sepcific(ID)/ or any used params
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// Route to update a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then (resultFromController => res.send(resultFromController));
})

// Route to get specific tasks
router.get("/:id", (req, res) => {
	taskController.getTasks(req.params.id).then(resultFromController => res.send (
		resultFromController));
})

// Route to update the status of a task
router.put("/:id/:status", (req, res) => {
	taskController.updateStatus(req.params.id, req.params.status).then (resultFromController => res.send(resultFromController));
})


// export the router object to be used in index.js
module.exports = router;