// Contain information about the server

// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute")

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
mongoose.connect("mongodb+srv://jeniezuitt:admin@cluster0.vsejk.mongodb.net/b177-to-do?retryWrites=true&w=majority", {
		useNewUrlParser:true,
		useUnifiedTopology: true
});

app.use("/tasks", taskRoute);

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

// Create controller
// Create route

// console.log(error); - > terminal
// return false -> postman