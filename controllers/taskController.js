// Controllers contains instructions oh how your API will perform its intended tasks

// import model and access
const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	// access Task model and find
	return Task.find({}).then(result => {
		return result;
	})
}

// Controller function for creating a task
module.exports.createTask = (requestBody) => {
	// Create a task object based on the mongoose model "Task"
	let newTask = new Task({
		// Set the "name" property with the value received from the client
		name: requestBody.name
	})
	// Saving new Task
	return newTask.save().then ((task, error) => {
		if(error){
			console.log(error);
			return false; //
		}
		// Save is successful
		else {
			return task;
		}
	})
}

// Create a controller function for deleting a task
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

// Create a controller function for updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		} 
		// reassign nameTask from result to newContent
		result.name = newContent.name;

		// Save the updated result in the MongoDB database
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else {
				return updatedTask;
			}
		})
	})
}

// Controller function for getting specific tasks
module.exports.getTasks = (taskId) => {
	// access Task model and find
	return Task.findById(taskId).then(result => {
		return result;
	})
}

// Create a controller function for updating the status of a task
module.exports.updateStatus = (taskId, taskNewStatus) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		} 
		// reassign nameTask from result to newContent
		result.status = taskNewStatus;

		// Save the updated result in the MongoDB database
		return result.save().then((updatedStatus, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else {
				return updatedStatus;
			}
		})
	})
}